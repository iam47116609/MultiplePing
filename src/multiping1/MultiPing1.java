/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiping1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gllado
 */
public class MultiPing1 {

    public static String[] results = new String[256];
    public static int progress = 0;

    public static void main(String[] args) throws InterruptedException {

        Scanner sc = new Scanner(System.in);
        System.out.println("Escribe los tres primeros octetos con sus correspondientes puntos."
                + "\n Un ejemplo sería 10.10.20. o 192.168.0.");

        String ipButLast = sc.nextLine();
        Thread t;
        ExecutorService es = Executors.newCachedThreadPool();
        System.out.println("Running...");
        //startProgressBar();
        for (int i = 0; i <= 255; i++) {
            t = new Thread(new PingThread(ipButLast + i , i));
            es.execute(t);
        }
        es.shutdown();
        boolean finished = es.awaitTermination(1, TimeUnit.MINUTES);
        if (finished) {
            writeAllToFile(ipButLast);
        }

    }

    public static void startProgressBar() {
        
        Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                if (progress%25==0) {
                    int i=0;
                    i+=10;
                    System.out.println(i+"%");
                }
            }
        }, 0, 1000);
    }

    private static void writeAllToFile(String ipButLast) {
        System.out.println("Ending...");
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(new File(ipButLast+"X")));
            for (int i = 1; i < results.length; i++) {
                writer.write(results[i]);
            }
        } catch (IOException ex) {
            Logger.getLogger(PingThread.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(PingThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}

class PingThread implements Runnable {

    String ip;
    int lastip;

    public PingThread(String ip, int lastip) {
        this.ip = ip;
        this.lastip = lastip;
    }

    @Override
    public void run() {
        Process p;
        StringBuilder sb = new StringBuilder();

        try {
            p = Runtime.getRuntime().exec("ping " + ip);
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = reader.readLine();
            while (line != null) {
                sb.append(line + "\n");
                line = reader.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(PingThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        MultiPing1.results[lastip] = sb.toString();
        //MultiPing1.progress++;

    }

}
